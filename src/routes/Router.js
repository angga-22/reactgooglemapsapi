import { BrowserRouter as Router, Routes, Route } from "react-router-dom";
import Home from "../pages/home";
import About from "../pages/about";
import Career from "../pages/career";
import NavBar from "../components/NavBar";
const RouterSet = () => (
  <Router>
    <NavBar />
    <Routes>
      <Route path="/" exact element={<Home />} />
      <Route path="/about" exact element={<About />} />
      <Route path="/career" exact element={<Career />} />
    </Routes>
  </Router>
);
export default RouterSet;
