export const iconStyle = {
  icon: {
    width: "100%",
  },
  size: {
    fontSize: "inherit",
  },
  avatar: {
    width: "100%",
    height: "100%",
  },
};
