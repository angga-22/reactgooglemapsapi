import { Icon, Avatar, IconButton } from "@mui/material";
import SearchIcon from "@mui/icons-material/Search";
import MenuIcon from "@mui/icons-material/Menu";
import { iconStyle } from "./style";

const Icons = ({ type, size, handleClick = null }) => (
  <IconButton color="inherit" size={size} onClick={handleClick}>
    <Icon fontSize={size} sx={iconStyle.icon}>
      {type === "search" && <SearchIcon sx={iconStyle.size} />}
      {type === "menu" && <MenuIcon sx={iconStyle.size} />}
      {type === "avatar" && (
        <Avatar
          alt="icon"
          size="large"
          src={require("../../assets/images/user.jpg")}
          sx={iconStyle.avatar}
        />
      )}
      {/* // underneath here there will be another icons  */}
    </Icon>
  </IconButton>
);
export default Icons;
