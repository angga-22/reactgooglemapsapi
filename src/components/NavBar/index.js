import { useState } from "react";
import {
  AppBar,
  Box,
  Toolbar,
  Typography,
  Menu,
  Container,
  Button,
  MenuItem,
  CardMedia,
} from "@mui/material";
import Icons from "../Icons";
import { Link } from "react-router-dom";
const menus = [
  {
    label: "Home",
    to: "/",
  },
  {
    label: "About",
    to: "/about",
  },
  {
    label: "Career",
    to: "/career",
  },
];

const NavBar = () => {
  const [anchorElNav, setAnchorElNav] = useState(null);

  const OpenNavMenu = (event) => {
    setAnchorElNav(event.currentTarget);
  };

  const CloseNavMenu = () => {
    setAnchorElNav(null);
  };

  return (
    <AppBar
      position="static"
      sx={{
        backgroundColor: "#ffc625",
      }}
    >
      <Container maxWidth="xl">
        <Toolbar disableGutters>
          <CardMedia
            sx={{ mr: 2, width: 200, display: { xs: "none", md: "flex" } }}
            component="img"
            image={require("../../assets/images/logo.png")}
            alt="logo"
          />
          <Box sx={{ flexGrow: 1, display: { xs: "flex", md: "none" } }}>
            <Icons type="menu" handleClick={OpenNavMenu} />
            <Menu
              id="menu-appbar"
              anchorEl={anchorElNav}
              anchorOrigin={{
                vertical: "bottom",
                horizontal: "left",
              }}
              keepMounted
              transformOrigin={{
                vertical: "top",
                horizontal: "left",
              }}
              open={Boolean(anchorElNav)}
              onClose={CloseNavMenu}
              sx={{
                display: { xs: "block", md: "none" },
              }}
            >
              {menus.map((page) => (
                <MenuItem key={page} onClick={CloseNavMenu}>
                  <Typography textAlign="center">
                    <Link
                      style={{
                        textDecoration: "none",
                        color: "#000",
                      }}
                      to={page.to}
                    >
                      {page.label}
                    </Link>
                  </Typography>
                </MenuItem>
              ))}
            </Menu>
          </Box>
          <Typography
            variant="h6"
            noWrap
            component="div"
            sx={{ flexGrow: 1, display: { xs: "flex", md: "none" } }}
          >
            <CardMedia
              sx={{
                width: 150,
              }}
              component="img"
              image={require("../../assets/images/logo.png")}
              alt="logo"
            />
          </Typography>

          <Box sx={{ flexGrow: 1, display: { xs: "none", md: "flex" } }}>
            {menus.map((page) => (
              <Button
                key={page}
                onClick={CloseNavMenu}
                sx={{ my: 2, pl: 10, display: "block" }}
              >
                <Link
                  style={{
                    textDecoration: "none",
                    color: "#000",
                  }}
                  to={page.to}
                >
                  {page.label}
                </Link>
              </Button>
            ))}
          </Box>
          <Box sx={{ flexGrow: 0 }}>
            <Icons type="avatar" size="large" />
          </Box>
        </Toolbar>
      </Container>
    </AppBar>
  );
};
export default NavBar;
