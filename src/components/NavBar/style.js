export const headerStyle = {
  header: {
    width: "100%",
    backgroundColor: "tomato",
  },
  ul: {
    listStyle: "none",
    display: "flex",
    justifyContent: "space-around",
  },
  li: {
    padding: "10px",
    textDecoration: "none",
    color: "white",
  },
};
