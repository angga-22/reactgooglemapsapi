import React from "react";
import { Input, InputLabel } from "@mui/material";
import { inputStyle } from "./style";
const InputComp = ({
  type,
  name,
  placeholder,
  value,
  label,
  handleChange,
  isError,
  isLabel = false,
  ...props
}) => (
  <>
    {isLabel && (
      <InputLabel
        sx={{
          fontWeight: "bold",
          fontSize: "14px",
          mb: "8px",
          textTransform: "capitalize",
        }}
      >
        {label}
      </InputLabel>
    )}
    <Input
      sx={inputStyle.input}
      type={type}
      onChange={handleChange}
      name={name}
      value={value}
      placeholder={placeholder}
      {...props}
    />
    {isError && <div className="error">The input value is invalid</div>}
  </>
);

export default React.memo(InputComp);
