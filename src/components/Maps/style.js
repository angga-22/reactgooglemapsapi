export const mapStyle = {
  inputWrapper: {
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
    justifyContent: "center",
    maxWidth: "500px",
    margin: "1rem auto",
    borderTopRightRadius: "5px",
    borderTopLeftRadius: "5px",
    padding: "1rem 0",
    backgroundColor: "#ededed",
  },
  searchResult: {
    padding: "8px 0 8px 7px",
    borderBottom: "1px solid #e0e0e0",
    width: "100%",
  },
};
