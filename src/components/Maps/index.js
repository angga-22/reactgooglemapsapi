import React, { useEffect, useState } from "react";
import { Box, Typography } from "@mui/material";
import { useDispatch, useSelector } from "react-redux";
import InputComp from "../../components/Input";
import InputAdornment from "@mui/material/InputAdornment";
import Icons from "../../components/Icons";
import { Map, Marker, GoogleApiWrapper } from "google-maps-react";
import PlacesAutocomplete from "react-places-autocomplete";
import {
  setAddress,
  selectedLocation,
  setSuggestions,
} from "../../store/reducer/locationSlice";
import Loading from "../Loading";
import { mapStyle } from "./style";

const Maps = () => {
  const [searchResult, setSearchResult] = useState([]);
  const dispatch = useDispatch();
  const { address, mapCenter, isLoading, suggestions } = useSelector(
    (state) => state.location
  );
  useEffect(() => {
    dispatch(setSuggestions(searchResult));
  }, [searchResult]);

  return (
    <>
      <PlacesAutocomplete
        value={address}
        onChange={(text) => dispatch(setAddress(text))}
        onSelect={() => dispatch(selectedLocation(address))}
      >
        {({ getInputProps, suggestions, getSuggestionItemProps, loading }) => {
          setSearchResult(suggestions);
          return (
            <Box sx={mapStyle.inputWrapper}>
              <InputComp
                {...getInputProps()}
                isLabel="true"
                label="search your location"
                type="text"
                name="text"
                placeholder="City, Province or any places, eg: London"
                startAdornment={
                  <InputAdornment position="start">
                    <Icons type="search" size="small" />
                  </InputAdornment>
                }
              />
              {loading && <Loading />}
              {suggestions.map((item) => {
                return (
                  <div {...getSuggestionItemProps(item)} key={item.placeId}>
                    <Typography sx={mapStyle.searchResult}>
                      {item.description}
                    </Typography>
                  </div>
                );
              })}
            </Box>
          );
        }}
      </PlacesAutocomplete>
      <Box>
        <Map
          google={window.google}
          initialCenter={{
            lat: mapCenter.lat,
            lng: mapCenter.lng,
          }}
          center={{
            lat: mapCenter.lat,
            lng: mapCenter.lng,
          }}
        >
          <Marker
            position={{
              lat: mapCenter.lat,
              lng: mapCenter.lng,
            }}
          />
        </Map>
      </Box>
    </>
  );
};

export default React.memo(
  GoogleApiWrapper({
    apiKey: "AIzaSyDTfcknlZY-Rdq4S28Mk5rAcyO79ds5Q9E",
  })(Maps)
);
