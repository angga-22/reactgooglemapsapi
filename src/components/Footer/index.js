import { footerStyle } from "./style";
const Footer = () => (
  <footer style={footerStyle.footer}>
    <p>
      Copyright © 2022 made with &#10084; by <strong> Angga Saputra </strong>{" "}
    </p>
  </footer>
);

export default Footer;
