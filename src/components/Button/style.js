export const buttonStyle = {
  button: {
    display: "inline-block",
    fontFamily: "inherit",
    width: "300px",
    height: "45px",
    backgroundColor: "red",
    borderRadius: "10px",
    fontWeight: "bold",
    color: "#fff",
  },
};
