import { buttonStyle } from "./style";
import Button from "@mui/material/Button";
const BtnPrimary = ({ name, onClick, type }) => (
  <Button
    sx={buttonStyle.button}
    variant="contained"
    type={type}
    onClick={onClick}
  >
    {name}
  </Button>
);
export default BtnPrimary;
