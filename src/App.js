import * as React from "react";
import Layout from "./layout";
import RouterSet from "./routes/Router.js";

const App = () => (
  <>
    <Layout>
      <RouterSet />
    </Layout>
  </>
);
export default App;
