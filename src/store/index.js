import { configureStore } from "@reduxjs/toolkit";
import locationSlice from "./reducer/locationSlice";
export const store = configureStore({
  reducer: {
    location: locationSlice,
  },
});
