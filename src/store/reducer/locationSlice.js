import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";
import { geocodeByAddress, getLatLng } from "react-places-autocomplete";
const initialState = {
  suggestions: [],
  isLoading: false,
  address: "",
  showingInfoWindow: false,
  activeMarker: {},
  selectedPlace: {},
  mapCenter: {
    lat: -6.2087634,
    lng: 106.845599,
  },
};

export const selectedLocation = createAsyncThunk(
  "location/selectedLocation",
  async (address) => {
    const results = await geocodeByAddress(address);
    const latLng = await getLatLng(results[0]);
    return {
      location:
        {
          lat: latLng.lat,
          lng: latLng.lng,
        } ?? {},
    };
  }
);

const location = createSlice({
  name: "locationSlice",
  initialState,
  reducers: {
    setAddress: (state, action) => {
      state.address = action.payload;
    },
    setSuggestions: (state, action) => {
      state.suggestions.push(action.payload);
    },
  },
  extraReducers: {
    [selectedLocation.pending]: (state) => {
      state.isLoading = true;
    },
    [selectedLocation.fulfilled]: (state, action) => {
      state.isLoading = false;
      state.mapCenter = action.payload.location;
      state.address = "";
    },
    [selectedLocation.rejected]: (state) => {
      state.isLoading = false;
    },
  },
});
export const { setAddress, setSuggestions } = location.actions;
export default location.reducer;
